//
//  Constants.swift
//  LGC Country App
//
//  Created by mark jones on 20/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

struct Constants {

    struct API {
        static let countryListAllURL = "https://restcountries-v1.p.rapidapi.com/all"
    }

    struct APIKeys {
        static let rapidAPIKey = "de9a41d015mshde699b964bcfc34p1cde80jsn235c82adf824"
        static let hostAPIKey = "restcountries-v1.p.rapidapi.com"
    }

    struct APIHeaderKey {
        static let apiKeyHeaderKey = "x-rapidapi-key"
        static let apiHostHeaderKey = "x-rapidapi-host"
    }

    struct FilePaths {
        static var contentDataFilePath: URL? {
            guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
                return nil
            }
            return url.appendingPathComponent("CountryData")
        }
    }

    struct AppKeys {
        static let countriesListViewTitle = "Countries list"
        static let countriesListTableViewCellID = "CountryTableViewCellID"
        static let favouriteAddIcon = "favouriteAdd"
        static let favouriteRemoveIcon = "favouriteRemove"
    }

    struct Layout {
        static let cellHorizontalPadding: CGFloat = 25.0
        static let cellVerticalPadding: CGFloat = 7.0
        static let cellStackViewSpacing: CGFloat = 2.0
    }

    struct strings {
        static let capitalTitleDefault = "no capital"
        static let regionTitleDefault = "no region"
        static let errorAlertTitle = "Error"
        static let actionSheetTitle = "Additional Information"
    }
}
