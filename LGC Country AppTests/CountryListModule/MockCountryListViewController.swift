//
//  MockCountryListViewController.swift
//  LGC Country AppTests
//
//  Created by mark jones on 22/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation
@testable import LGC_Country_App

class MockCountryListViewController: CountryListViewController {
    var showCountryListCalled = false
    var showErrorMessageCalledWithMessage: String?

    override func showCountryList() {
        showCountryListCalled = true
    }

    override func showErrorMessage(_ message: String) {
        showErrorMessageCalledWithMessage = message
    }
}
