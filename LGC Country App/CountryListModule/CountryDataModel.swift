//
//  CountryDataModel.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation

struct CountryDataModel: Codable {
    var name: String
    var region: String?
    var capital: String?
    var population: Int?
    var area: Double?
    var borders: [String]?
    var timezones: [String]?
    var favourite: Bool?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        region = try values.decode(String?.self, forKey: .region)
        capital = try values.decode(String?.self, forKey: .capital)
        population = try values.decode(Int?.self, forKey: .population) ?? 0
        area = try values.decode(Double?.self, forKey: .area) ?? 0.0
        borders = try values.decode([String]?.self, forKey: .borders) ?? []
        timezones = try values.decode([String]?.self, forKey: .timezones) ?? []
        favourite = try? values.decode(Bool?.self, forKey: .favourite) ?? false
    }

    init(name: String,
         region: String?,
         capital: String?,
         population: Int?,
         area: Double?,
         borders: [String]?,
         timezones: [String]?,
         favourite: Bool?) {
        self.name = name
        self.region = region
        self.capital = capital
        self.population = population
        self.area = area
        self.borders = borders
        self.timezones = timezones
        self.favourite = favourite
    }
}

extension CountryDataModel {
    func additionalDataString() -> String {
        var info = ""
        info.append("\("Population".localized()): \(self.population ?? 0)")
        info.append("\n\("Area".localized()): \(self.area ?? 0)")
        info.append("\n\("Bordering countries".localized()): \(self.borders?.count ?? 0)")
        info.append("\n\("Different timezones".localized()): \(self.timezones?.count ?? 0)")
        return info
    }
}
