//
//  MockCountryListInteractor.swift
//  LGC Country AppTests
//
//  Created by mark jones on 22/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation
@testable import LGC_Country_App

class MockCountryListInteractor: CountryListInteractor {
    var fetchCountryListDataCalled = false
    var cachedDataOnMock: [CountryDataModel] = []

    override func fetchCountryListData() {
        fetchCountryListDataCalled = true
    }

    override func cacheData(_ data: [CountryDataModel]) {
        cachedDataOnMock = data
    }
}
