//
//  UILabel+Extensions.swift
//  LGC Country App
//
//  Created by mark jones on 20/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

extension UILabel {
    func configureForTitle() {
        textColor = .blue
        font = UIFont.boldSystemFont(ofSize: 17)
        textAlignment = .left
        numberOfLines = 0
    }

    func configureForSubTitle() {
        textColor = .black
        font = UIFont.systemFont(ofSize: 15)
        textAlignment = .left
        numberOfLines = 0
    }

    convenience init(_ type: LabelType) {
        self.init()
        switch type {
        case .TitleLabel:
            configureForTitle()
        case .SubTitleLabel:
            configureForSubTitle()
        }
    }
}

enum LabelType {
    case TitleLabel
    case SubTitleLabel
}
