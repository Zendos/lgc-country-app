//
//  CountryListProtocols.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

// MARK: View Implements. The contract expected by presenter.
protocol PresenterToViewCountryListProtocol: class {
    var presenter: ViewToPresenterCountryListProtocol? { get set }
    
    func showCountryList()
    func showErrorMessage(_ message: String)
}

// MARK: Presenter Implements. The contract expected by view.
protocol ViewToPresenterCountryListProtocol: class {
    var view: PresenterToViewCountryListProtocol? { get set }
    var interactor: PresenterToInteractorCountryListProtocol? { get set }
    var router: PresenterToRouterCountryListProtocol? { get set }
    var countryListData: [CountryDataModel] { get set }

    func viewDidLoad()
    func retrieveInfoForCountry(_ index: Int) -> String
    func handleFavouriting(_ index: Int)
}

// MARK: Interactor Implements. The contract expected by presenter.
protocol PresenterToInteractorCountryListProtocol: class {
    var presenter: InteractorToPresenterCountryListProtocol? { get set }
    var dataManager: InteractorToDataManagerCountryListProtocol? { get set }

    func fetchCountryListData()
    func cacheData(_ data: [CountryDataModel])
}

// MARK: Presenter Implements. The contract expected by the interactor.
protocol InteractorToPresenterCountryListProtocol: class {
    func didFetchCountryList(_ countryList: [CountryDataModel])
    func errorFetchingCountryList(_ message: String)
}

// MARK: Router Implements: The contract expected by the presenter.
protocol PresenterToRouterCountryListProtocol: class {
}

// MARK: DataManager Implements. contract expected by the Interactor.
protocol InteractorToDataManagerCountryListProtocol: class {
    func sendRequest(request: URLRequest, completion: @escaping(Swift.Result<[CountryDataModel], ErrorModel>) -> Void)
    func retrieveCacheData() -> [CountryDataModel]? 
    func cacheData(_ data: [CountryDataModel], completion: @escaping ((ErrorModel?) -> Void))
}

protocol TableViewCellButtonDelegate: class {
    func handleCellFavourite(cell: UITableViewCell)
}

