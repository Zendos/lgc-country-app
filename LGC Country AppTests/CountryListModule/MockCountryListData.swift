//
//  MockCountryListData.swift
//  LGC Country AppTests
//
//  Created by mark jones on 22/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

@testable import LGC_Country_App

class MockCountryListData {
    lazy var fullData: [CountryDataModel] = {
        let mockCountry1 = CountryDataModel(name: "mockCountry1Name",
                                            region: "mockCountry1Region",
                                            capital: "mockCountry1Capital",
                                            population: 1000,
                                            area: 100.0,
                                            borders: ["mockBorder1", "mockBorder2"],
                                            timezones: ["mockTimezone1", "mockTimezone2"],
                                            favourite: false)
        return [mockCountry1]
    }()

    lazy var partialData: [CountryDataModel] = {
        let mockCountry1 = CountryDataModel(name: "mockCountry1Name",
                                            region: "mockCountry1Region",
                                            capital: "mockCountry1Capital",
                                            population: nil,
                                            area: nil,
                                            borders: ["mockBorder1", "mockBorder2"],
                                            timezones: ["mockTimezone1", "mockTimezone2"],
                                            favourite: false)
        return [mockCountry1]
    }()
}
