//
//  MainNavigationController.swift
//  LGC Country App
//
//  Created by mark jones on 20/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.barTintColor = .blue
        navigationBar.tintColor = .white
    }
}
