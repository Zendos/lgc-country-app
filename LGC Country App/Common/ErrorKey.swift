//
//  ErrorKey.swift
//  LGC Country App
//
//  Created by mark jones on 19/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation

enum ErrorKey: String {
    case parsing = "Parsing Failed"
    case unauthorised = "Unauthorised"
    case unavailable = "Service is unavailable"
    case caching = "Caching Failed"
    case unknown = "Unknown error"
}
