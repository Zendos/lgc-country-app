//
//  CountryTableViewCell.swift
//  LGC Country App
//
//  Created by mark jones on 20/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    static let cellID = Constants.AppKeys.countriesListTableViewCellID
    weak var tableViewCellDelegate: TableViewCellButtonDelegate?
    var button: UIButton?

    private let titleLabel = UILabel(.TitleLabel)
    private let firstSubTitleLabel = UILabel(.SubTitleLabel)
    private let secondSubTitleLabel = UILabel(.SubTitleLabel)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .yellow
        setupStackView()
        setupFavouriteButton()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupStackView() {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, firstSubTitleLabel, secondSubTitleLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        stackView.spacing = Constants.Layout.cellStackViewSpacing
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topAnchor,
                                       constant: Constants.Layout.cellVerticalPadding).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor,
                                        constant: Constants.Layout.cellHorizontalPadding).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor,
                                          constant: -Constants.Layout.cellVerticalPadding).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor,
                                         constant: -Constants.Layout.cellHorizontalPadding).isActive = true
    }

    private func setupFavouriteButton() {
        button = UIButton(frame: CGRect(x: 0, y: 0, width: frame.height, height: frame.height))
        button?.addTarget(self, action: #selector(favouriteItemAction(_:)), for: .touchUpInside)
        accessoryView = button
    }

    func configure(name: String, capital: String, region: String, favourite: Bool) {
        titleLabel.text = name
        firstSubTitleLabel.text = capital.count > 0 ? capital : Constants.strings.capitalTitleDefault.localized()
        secondSubTitleLabel.text = region.count > 0 ? region : Constants.strings.regionTitleDefault.localized()
        if favourite == true {
            button?.setImage(UIImage(named: Constants.AppKeys.favouriteRemoveIcon), for: .normal)
        } else {
            button?.setImage(UIImage(named: Constants.AppKeys.favouriteAddIcon), for: .normal)
        }
    }

    @objc func favouriteItemAction(_ sender: UIButton) {
        tableViewCellDelegate?.handleCellFavourite(cell: self)
    }
}
