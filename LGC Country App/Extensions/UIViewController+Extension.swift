//
//  UIViewController+Extension.swift
//  LGC Country App
//
//  Created by mark jones on 21/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(_ message: String) {
        let title = Constants.strings.errorAlertTitle.localized()
        let alertView = UIAlertController(title: "\(title):", message: message, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
    }

    func showActionSheet(_ message: String, completion: ((UIAlertAction) -> Void)? = nil) {
        let title = Constants.strings.actionSheetTitle.localized()
        let actionSheet = UIAlertController(title: "\(title):", message: message, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "OK", style: .default, handler: completion))
        self.present(actionSheet, animated: true, completion: nil)
    }
}
