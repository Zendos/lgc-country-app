//
//  CountryListPresenterTests.swift
//  LGC Country AppTests
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import XCTest
@testable import LGC_Country_App

class CountryListPresenterTests: XCTestCase {
    var mockInteractor: MockCountryListInteractor!
    var mockView: MockCountryListViewController!
    var sut: CountryListPresenter!

    override func setUpWithError() throws {
        mockInteractor = MockCountryListInteractor()
        mockView = MockCountryListViewController()
        sut = CountryListPresenter()
        sut.interactor = mockInteractor
        sut.view = mockView
        sut.countryListData = MockCountryListData().fullData
    }

    override func tearDownWithError() throws {
        mockInteractor = nil
        mockView = nil
        sut = nil
    }

    // MARK:- ViewToPresenterCountryListProtocol Tests

    func testViewDidLoad() {
        sut.viewDidLoad()
        XCTAssertTrue(mockInteractor.fetchCountryListDataCalled)
    }

    func testRetrieveInfoForCountry_fullDataModel() {
        let sutInfoResult = sut.retrieveInfoForCountry(0)
        let expectedInfoResult = "Population: 1000\nArea: 100.0\nBordering countries: 2\nDifferent timezones: 2"

        XCTAssertEqual(sutInfoResult, expectedInfoResult)
    }

     func testRetrieveInfoForCountry_partialDataModel() {
        sut.countryListData = MockCountryListData().partialData
        let sutInfoResult = sut.retrieveInfoForCountry(0)
        let expectedInfoResult = "Population: 0\nArea: 0.0\nBordering countries: 2\nDifferent timezones: 2"

        XCTAssertEqual(sutInfoResult, expectedInfoResult)
    }

    func testHandleFavouriting_falseToTrue() throws {
        sut.handleFavouriting(0)
        let sutCountryModel = try XCTUnwrap(sut.countryListData.first)
        let favourited = try XCTUnwrap(sutCountryModel.favourite)

        XCTAssertTrue(favourited)
    }

    func testHandleFavouriting_trueToFalse() throws {
        sut.countryListData[0].favourite = true
        sut.handleFavouriting(0)
        let sutCountryModel = try XCTUnwrap(sut.countryListData.first)
        let favourited = try XCTUnwrap(sutCountryModel.favourite)

        XCTAssertFalse(favourited)
    }


    func testHandleFavouriting_callsInteractorCache() throws {
        sut.handleFavouriting(0)
        let sutCountryModel = try XCTUnwrap(sut.countryListData.first)
        let expectedCountryName = sutCountryModel.name
        let expectedCountryCapital = sutCountryModel.capital
        let expectedCountryRegion = sutCountryModel.region
        let expectedCountryPopulation = sutCountryModel.population
        let expectedCountryArea = sutCountryModel.area
        let expectedCountryFavourite = sutCountryModel.favourite

        XCTAssertEqual(sut.countryListData.count, mockInteractor.cachedDataOnMock.count)
        let cachedCountry = try XCTUnwrap(mockInteractor.cachedDataOnMock.first)
        XCTAssertEqual(cachedCountry.name, expectedCountryName)
        XCTAssertEqual(cachedCountry.capital, expectedCountryCapital)
        XCTAssertEqual(cachedCountry.region, expectedCountryRegion)
        XCTAssertEqual(cachedCountry.population, expectedCountryPopulation)
        XCTAssertEqual(cachedCountry.area, expectedCountryArea)
        XCTAssertEqual(cachedCountry.favourite, expectedCountryFavourite)
    }

    // MARK:- InteractorToPresenterCountryListProtocol Tests

    func testDidFetchCountryList_fullReplacedWithPartialData() throws {
        sut.didFetchCountryList(MockCountryListData().partialData)
        let sutCountryModel = try XCTUnwrap(sut.countryListData.first)

        XCTAssertNil(sutCountryModel.population)
        XCTAssertNil(sutCountryModel.area)
        XCTAssertTrue(mockView.showCountryListCalled)
    }

    func testErrorFetchingCountryList() {
        let sutErrorMessage = "sutErrorMessage"
        sut.errorFetchingCountryList(sutErrorMessage)

        XCTAssertEqual(sutErrorMessage, mockView.showErrorMessageCalledWithMessage)
    }
}
