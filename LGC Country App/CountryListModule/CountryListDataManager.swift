//
//  CountryListDataManager.swift
//  LGC Country App
//
//  Created by mark jones on 17/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation

class CountryListDataManager: InteractorToDataManagerCountryListProtocol {

    func sendRequest(request: URLRequest, completion: @escaping(Swift.Result<[CountryDataModel], ErrorModel>) -> Void) {
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let response = response as? HTTPURLResponse else {
                completion(Result.failure(ErrorModel(ErrorKey.unknown.rawValue)))
                return
            }
            guard response.statusCode == 200 else {
                let errorModel: ErrorModel
                switch response.statusCode {
                case 401, 403:
                    errorModel = ErrorModel(ErrorKey.unauthorised.rawValue)
                case 500, 503:
                    errorModel = ErrorModel(ErrorKey.unavailable.rawValue)
                default:
                    errorModel = ErrorModel(ErrorKey.unknown.rawValue)
                }
                completion(Result.failure(errorModel))
                return
            }
            guard let data = data, let responseModel = try? JSONDecoder().decode([CountryDataModel].self, from: data) else {
                completion(Result.failure(ErrorModel(ErrorKey.parsing.rawValue)))
                return
            }
            completion(Result.success(responseModel))
        }.resume()
    }

    func cacheData(_ data: [CountryDataModel], completion: @escaping ((ErrorModel?) -> Void)) {
        let encoder = JSONEncoder()
        if let encodedModel = try? encoder.encode(data) {
            do {
                guard let filePath = Constants.FilePaths.contentDataFilePath else {
                    completion(ErrorModel(ErrorKey.caching.rawValue))
                    return
                }
                try encodedModel.write(to: filePath)
                completion(nil)
            } catch {
                completion(ErrorModel(ErrorKey.caching.rawValue))
            }
        }
    }

     func retrieveCacheData() -> [CountryDataModel]? {
         guard  let filePath = Constants.FilePaths.contentDataFilePath,
             let cachedData = try? Data(contentsOf: filePath),
         let decodedModel = try? JSONDecoder().decode([CountryDataModel].self, from: cachedData) else {
            return nil
         }
         return decodedModel
     }
}
