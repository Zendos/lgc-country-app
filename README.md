# Country List iOS Application

`Country List` is a Universal iOS application.  
It lists all countries on the initial country list screen alongside that countries region and capital.  
Tapping a country will reveal an actionSheet displaying additional country data.  
Countries can be favourited by tapping the favourite button for the specified country.  
The application is localised for both English and French.   
In its current state the application makes one initial request to the API on installation.  
Thereafter the received data is persisted offline and used within the application.

[![Platform](https://img.shields.io/cocoapods/p/TimerControl)](https://cocoapods.org/pods/TimerControl)

## Getting Started

Clone the project and open in Xcode 11 to run on device or simulator.

### Prerequisites

* iOS 13.5
* Xcode 11
  
## Architecture

### - [Architecture Drawing](https://bitbucket.org/Zendos/lgc-country-app/src/master/LGCArchitecture.png)

The application is built using VIPER architecture.  
The aim is to separate responsibility to individual classes.  
The component classes are: View, Interactor, Presenter, Entity (or datamodel) and Router (or wireFrame).  
The `view` should only manage the layout of the view which in this case is programatically constructed.  
The `Interactor` is the layer between the presenter and the required data.   
It is concerned with business logic related to the handling of data to and from the presenter.  
The `Presenter` is responsible for providing the view with its required data.   
It should also provide the logic required by any view interactions.  
The `Entity` is the dataModel.  
The `Router` is the component responsible for creating the module and for any navigation from the module view.  
Additional functionality is provided by `Service` classes and `Manager` classes.  
The aim is to improve testablility and extensibility in the application for future development.

## Requirements

- the application is a universal application.
- tableViewCells display country name, capital city and region.
- tapping an individual cell should display further information related to that country.  
The actionsheet should have a cancel button: in this case I have used the `.default` action style  
and labelled the button 'OK' which doesn't contravene Apples guidelines.  
[Apple Action Sheet Human Interface Guide](https://developer.apple.com/design/human-interface-guidelines/ios/views/action-sheets/)
- the application should have the ability to favourite countries.
- favourited countries should be persisted. 
- at least part of the project should be covered by unit testing. In this case the CountryListPresenter is fully unit tested.
- the UI should be constructed programatically. No xibs or storyboards.
- the application should be localized for at least two languages. In this case French and English. App strings are localized.
- a separate network layer should handle any API calls.
