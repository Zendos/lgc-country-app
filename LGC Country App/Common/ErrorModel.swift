//
//  ErrorModel.swift
//  LGC Country App
//
//  Created by mark jones on 19/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

class ErrorModel: Error {

    var messageKey: String
    var message: String {
        return messageKey.localized()
    }

    init(_ messageKey: String) {
        self.messageKey = messageKey
    }
}
