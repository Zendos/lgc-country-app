//
//  CountryListViewController.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

class CountryListViewController: UIViewController, PresenterToViewCountryListProtocol {
    var presenter: ViewToPresenterCountryListProtocol?
    var tableView = UITableView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.AppKeys.countriesListViewTitle.localized()
        setupTableView()
        setupActivityIndicatorView()
        activityIndicator.startAnimating()
        presenter?.viewDidLoad()
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CountryTableViewCell.self, forCellReuseIdentifier: CountryTableViewCell.cellID)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.backgroundColor = .yellow
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }

    private func setupActivityIndicatorView() {
        view.addSubview(activityIndicator)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }

    func showCountryList() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
        }
    }

    func showErrorMessage(_ message: String) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.showAlert(message)
        }
    }
}

extension CountryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = presenter?.countryListData.count ?? 1
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.cellID,
                                                 for: indexPath) as! CountryTableViewCell
        guard let country = presenter?.countryListData[indexPath.row] else { return cell }
        cell.configure(name: country.name,
                       capital: country.capital ?? "",
                       region: country.region ?? "",
                       favourite: country.favourite ?? false)
        cell.tableViewCellDelegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let info = presenter?.retrieveInfoForCountry(indexPath.row) else { return }
        showActionSheet(info) { _ in
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
}

extension CountryListViewController: TableViewCellButtonDelegate {
    func handleCellFavourite(cell: UITableViewCell) {
        guard let sourceIndexPath = tableView.indexPath(for: cell) else { return }
        presenter?.handleFavouriting(sourceIndexPath.row)
        tableView.reloadRows(at: [sourceIndexPath], with: .automatic)
    }
}
