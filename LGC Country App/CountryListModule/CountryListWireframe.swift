//
//  CountryListWireframe.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

class CountryListWireframe {
    static func createCountryListModule() -> UIViewController {
        let view = CountryListViewController()
        let presenter: ViewToPresenterCountryListProtocol & InteractorToPresenterCountryListProtocol = CountryListPresenter()
        let interactor: PresenterToInteractorCountryListProtocol = CountryListInteractor()
        let dataManager: InteractorToDataManagerCountryListProtocol = CountryListDataManager()

        view.presenter = presenter

        presenter.view = view
        presenter.interactor = interactor

        interactor.presenter = presenter
        interactor.dataManager = dataManager

        return view
    }
}
