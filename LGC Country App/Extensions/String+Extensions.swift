//
//  String+Extensions.swift
//  LGC Country App
//
//  Created by mark jones on 19/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: self)
    }
}
