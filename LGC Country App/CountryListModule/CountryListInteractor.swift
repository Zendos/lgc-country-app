//
//  CountryListInteractor.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import UIKit

class CountryListInteractor: PresenterToInteractorCountryListProtocol {
    var presenter: InteractorToPresenterCountryListProtocol?
    var dataManager: InteractorToDataManagerCountryListProtocol?

    private func countryListUrlRequest() -> URLRequest? {
        guard let url = URL(string: Constants.API.countryListAllURL) else { return nil }
        var request = URLRequest(url: url)
        request.setValue(Constants.APIKeys.hostAPIKey, forHTTPHeaderField: Constants.APIHeaderKey.apiHostHeaderKey)
        request.setValue(Constants.APIKeys.rapidAPIKey, forHTTPHeaderField: Constants.APIHeaderKey.apiKeyHeaderKey)
        return request
    }

    func fetchCountryListData() {
        if let cacheData = dataManager?.retrieveCacheData() {
            presenter?.didFetchCountryList(cacheData)
            return
        }
        guard let urlRequest = countryListUrlRequest() else { return }
        dataManager?.sendRequest(request: urlRequest) { [weak self] result in
            switch result {
            case Result.success(let response):
                self?.presenter?.didFetchCountryList(response)
                break
            case Result.failure(let error):
                self?.presenter?.errorFetchingCountryList(error.message)
                break
            }
        }
    }

    func cacheData(_ data: [CountryDataModel]) {
        dataManager?.cacheData(data) { [weak self] error in
            if let error = error {
                self?.presenter?.errorFetchingCountryList(error.message)
            }
        }
    }
}
