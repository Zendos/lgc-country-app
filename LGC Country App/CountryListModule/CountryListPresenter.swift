//
//  CountryListPresenter.swift
//  LGC Country App
//
//  Created by mark jones on 15/09/2020.
//  Copyright © 2020 mark jones. All rights reserved.
//

import Foundation

class CountryListPresenter: ViewToPresenterCountryListProtocol {
    weak var view: PresenterToViewCountryListProtocol?
    var interactor: PresenterToInteractorCountryListProtocol?
    var router: PresenterToRouterCountryListProtocol?
    var countryListData = [CountryDataModel]()

    func viewDidLoad() {
        interactor?.fetchCountryListData()
    }

    func retrieveInfoForCountry(_ index: Int) -> String {
        return countryListData[index].additionalDataString()
    }

    func handleFavouriting(_ index: Int) {
        countryListData[index].favourite = countryListData[index].favourite == true ? false : true
        interactor?.cacheData(countryListData)
    }
}

extension CountryListPresenter: InteractorToPresenterCountryListProtocol {
    func didFetchCountryList(_ countryList: [CountryDataModel]) {
        countryListData = countryList
        self.view?.showCountryList()
    }

    func errorFetchingCountryList(_ message: String) {
        view?.showErrorMessage(message.localized())
    }


}
